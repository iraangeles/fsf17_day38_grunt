module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
        productionHtml: {
            expand: true,
            cwd: 'source/client/',
            src: '*.html',
            dest: 'release/client/'
        },
        productionJS:{
            expand: true,
            cwd: 'source/client/',
            src: '*.js',
            dest: 'release/client/'            
        },
        productionCSS: {
            expand: true,
            cwd: 'source/client/',
            src: '**/*.less',
            dest: 'release/client/'
        }        
    },
    uglify: {
        productionJS: {
            options: {
                sourceMap: true
            },
            files: {
                'release/client/main.min.js': ['release/client/main.js']
            }

         }
        
    },
    less: {
        productionCSS: {
            files: {
                'release/client/css/main.css': 'release/client/css/main.less'
            }
        },
        devCSS: {
            files: {
                'source/client/css/main.css': 'source/client/css/main.less'
            }
        }
    } 


 });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');

  // Default task(s).
  grunt.registerTask('default', ['copy','uglify', 'less']);


};